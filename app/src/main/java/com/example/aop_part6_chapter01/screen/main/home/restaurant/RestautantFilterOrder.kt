package com.example.aop_part6_chapter01.screen.main.home.restaurant

enum class RestautantFilterOrder {

    FAST_DELIVERY, LOW_DELIVERY_TIP, DEFAULT, TOP_RATE

}