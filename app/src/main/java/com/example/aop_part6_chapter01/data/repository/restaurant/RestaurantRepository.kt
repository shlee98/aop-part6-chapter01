package com.example.aop_part6_chapter01.data.repository.restaurant

import com.example.aop_part6_chapter01.data.entity.location.LocationLatLngEntity
import com.example.aop_part6_chapter01.data.entity.restaurant.RestaurantEntity
import com.example.aop_part6_chapter01.screen.main.home.restaurant.RestaurantCategory

interface RestaurantRepository {

    suspend fun getList(
        restaurantCategory: RestaurantCategory,
        locationLatLngEntity: LocationLatLngEntity
    ): List<RestaurantEntity>

}