package com.example.aop_part6_chapter01.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aop_part6_chapter01.data.db.dao.FoodMenuBasketDao
import com.example.aop_part6_chapter01.data.db.dao.LocationDao
import com.example.aop_part6_chapter01.data.db.dao.RestaurantDao
import com.example.aop_part6_chapter01.data.entity.restaurant.RestaurantEntity
import com.example.aop_part6_chapter01.data.entity.location.LocationLatLngEntity
import com.example.aop_part6_chapter01.data.entity.restaurant.RestaurantFoodEntity

@Database(
    entities = [LocationLatLngEntity::class, RestaurantFoodEntity::class, RestaurantEntity::class],
    version = 1,
    exportSchema = false
)

abstract class ApplicationDatabase: RoomDatabase() {

    companion object {
        const val DB_NAME = "ApplicationDataBase.db"
    }

    abstract fun LocationDao(): LocationDao

    abstract fun FoodMenuBasketDao(): FoodMenuBasketDao

    abstract fun RestaurantDao(): RestaurantDao

}